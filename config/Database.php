<?php 
    class Database {
        private $host = "localhost";
        private $database_name = "citiesapidb";
        private $username = "root";
        private $password = "";
        public $conn;
        public function getConnection(){
            
            $this->conn =new mysqli($this->host, $this->username, $this->password,$this->database_name);
                
            return $this->conn;
        }
    }  
?>