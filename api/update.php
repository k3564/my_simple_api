<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: PUT");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/database.php';
    include_once '../class/Cities.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Cities($db);
    
    $data =  json_decode(file_get_contents('php://input'));
    
    $item->id = $data->id;
    
    $item->city = $data->city;
    $item->city_ascii = $data->city_ascii;
    $item->lat = $data->lat;
    $item->lng = $data->lng;
    $item->country = $data->country;
    $item->iso2 = $data->iso2;
    $item->iso3 = $data->iso3;
    $item->admin_name = $data->admin_name;
    $item->capital = $data->capital;
    $item->population = $data->population;
    $res=$item->updateCity();
    if($res["status"]){
        echo json_encode("Data updated.");
    } else{
        echo json_encode("Data could not be updated");
    }
?>
