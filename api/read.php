<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header('Access-Control-Allow-Methods:GET');
    
    include_once '../config/Database.php';
    include_once '../class/Cities.php';
    $database = new Database();
    $db = $database->getConnection();
    $items = new Cities($db);
    $stmt = $items->getCities();
    $itemCount = mysqli_num_rows($stmt);

    if($itemCount > 0){
         $i=0;
        while ($row =  mysqli_fetch_assoc($stmt)){
            $citiesArr[$i]['city']=$row['city'];
            $citiesArr[$i]['id']=$row['id'];
            $i++;
        }
        echo json_encode($citiesArr,JSON_PRETTY_PRINT);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
?>
