<?php
    header("Access-Control-Allow-Origin: *"); // request w/o auth
    header("Content-Type: application/json; charset=UTF-8"); 
    header("Access-Control-Allow-Methods: GET"); // allow GET methods
    header("Access-Control-Max-Age: 3600");// how long results can be cached in seconds
    //allow multiple headers
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    include_once '../config/database.php';
    include_once '../class/Cities.php';
    $database = new Database();
    $db = $database->getConnection();
    $item = new Cities($db);
    $item->city = isset($_GET['city']) ? $_GET['city'] : die(); //The isset function in PHP is used to determine whether a variable is set or not.
  
    $stmt=$item->getSingleCity();
    $itemCount = mysqli_num_rows($stmt);
    if($itemCount > 0){
     
        $i=0;
        while ($row =  mysqli_fetch_assoc($stmt)){
            $citiesArr[$i]['city']=$row['city'];
            $citiesArr[$i]['city_ascii']=$row['city_ascii'];
            $citiesArr[$i]['lat']=$row['lat'];
            $citiesArr[$i]['lng']=$row['lng'];
            $citiesArr[$i]['country']=$row['country'];
            $citiesArr[$i]['iso2']=$row['iso2'];
            $citiesArr[$i]['iso3']=$row['iso3'];
            $citiesArr[$i]['admin_name']=$row['admin_name'];
            $citiesArr[$i]['capital']=$row['capital'];
            $citiesArr[$i]['population']=$row['population'];
            $citiesArr[$i]['id']=$row['id'];
            $i++;
        }
      
        http_response_code(200);
        echo json_encode($citiesArr,JSON_PRETTY_PRINT);
    }
      
    else{
        http_response_code(404);
        echo json_encode("City not found.");
    }
?>
