<?php
    class Cities{
        // Connection
        private $conn;
        // Table
        private $db_table = "worldcities";
        // Columns
        public $id, $city, $city_ascii, $lat, $lng, $country, $iso2, $iso3, $admin_name, $capital, $population;
        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }
        // GET 20 cities
        public function getCities(){
            $sqlQuery = "SELECT city,id FROM " . $this->db_table . " ORDER BY population LIMIT 20 ";
            $res = mysqli_query($this->conn, $sqlQuery);
            return $res;
        }
        
        // READ single city data
        public function getSingleCity(){
            $sqlQuery = "SELECT * FROM ". $this->db_table .' WHERE city = "'. $this->city. '"';
            $dataRow=mysqli_query($this->conn, $sqlQuery);
            return $dataRow;
        }        

        // Add new city info
        public function createCity(){
            $values='"'.
            $this->city .'","'.$this->city_ascii .'","'.
                    $this->lat .'","'.$this->lng .'","'.
                    $this->country .'","'.$this->iso2 .'","'.
                    $this->iso3 .'","'.$this->admin_name .'","'.
                    $this->capital .'","'.$this->population.'"';
            
            $sqlQuery = "INSERT INTO ". $this->db_table ." (city,city_ascii,lat,lng,country,iso2,iso3,admin_name,capital,population) VALUES (". $values.")";
            
                    $result=mysqli_query($this->conn, $sqlQuery);
                    if($result){
                        $resultData = array('status' => true, 'message' => 'Inserted Successfully...');
                        
                        }else{
                            $resultData = array('status' => false, 'message' => 'Unable able to insert...');
                            
                        }
                    
                    return $resultData;
                }    
                
        // UPDATE
        public function updateCity(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        city = \"".$this->city ."\", 
                        city_ascii = \"".$this->city_ascii ."\",
                        lat = \"".$this->lat ."\",
                        lng = \"".$this->lng ."\",
                        country = \"".$this->country ."\",
                        iso2 = \"".$this->iso2 ."\",
                        iso3 = \"".$this->iso3 ."\",
                        admin_name = \"".$this->admin_name ."\",
                        capital = \"".$this->capital ."\",
                        population = \"".$this->population ."\" 
                    WHERE 
                        id =" . $this->id;
        
                        $result=mysqli_query($this->conn, $sqlQuery);
                        if($result){
                            $resultData = array('status' => true, 'message' => 'Data Updated Successfully...');
                            
                            }else{
                                $resultData = array('status' => false, 'message' => 'Unable able to updated...');
                                
                            }
                    return $resultData;
                }

        // DELETE
        function deleteCity(){

            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE city =\"". $this->city. "\" AND id=" .$this->id;
            
            $result=mysqli_query($this->conn, $sqlQuery);
            if($result){
                $resultData = array('status' => true, 'message' => 'Deleted Successfully...');
                
                }else{
                    $resultData = array('status' => false, 'message' => 'Unable able to delete...');
                    
                }
            return $resultData;
            
        }
    }
?>
